
# Configuration for using Moxa in "TCP Socket" mode
drvAsynIPPortConfigure("$(PORTNAME)","$(IP):$(PORT)",0,0,0)

# Record database
dbLoadRecords(lamseDosRM1001B.template,"PORTNAME=$(PORTNAME),PREFIX=$(PREFIX)")
